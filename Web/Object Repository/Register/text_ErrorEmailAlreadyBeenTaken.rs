<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_ErrorEmailAlreadyBeenTaken</name>
   <tag></tag>
   <elementGuidId>429b591f-7709-4926-8fa1-e64b156a7401</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='new_user']/div[2]/div[3]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-text.text-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4a9d7c11-cbce-4ed3-86b3-1f9d20f081c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-text text-danger</value>
      <webElementGuid>a2f599e6-a409-4eee-a097-e44a7cf8ed63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Email has already been taken
              </value>
      <webElementGuid>007023ac-fb2c-4152-897f-273fc01bff2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;new_user&quot;)/div[@class=&quot;field mb-4&quot;]/div[@class=&quot;form-text text-danger&quot;]</value>
      <webElementGuid>e19a4800-ce83-45d6-8069-3995734020f6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='new_user']/div[2]/div[3]</value>
      <webElementGuid>1af58359-7444-49ff-88a7-5428ffda807b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::div[2]</value>
      <webElementGuid>3fff750f-29e6-41fa-8e6d-4355476332d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::div[4]</value>
      <webElementGuid>497d0034-a124-4b67-b730-8676fb975edb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::div[1]</value>
      <webElementGuid>3be728e8-1176-4885-ae67-532414ae84eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk di sini'])[1]/preceding::div[3]</value>
      <webElementGuid>79d2746a-29ee-4b42-9791-cd224df85544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email has already been taken']/parent::*</value>
      <webElementGuid>f2e048db-eed3-4752-b4df-65d7b66e92f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>1c7a20e5-f44a-4cca-9c16-3fbeb4fc2df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Email has already been taken
              ' or . = '
                Email has already been taken
              ')]</value>
      <webElementGuid>d8cd7be8-297b-41b0-b386-166d0825542f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
