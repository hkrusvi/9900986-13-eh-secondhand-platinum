<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Telusuri Kategori</name>
   <tag></tag>
   <elementGuidId>0433cb8a-0ad4-4a61-975e-1543628207cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h5.fw-bold.mb-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::h5[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>1b08c162-c65f-4a7b-9f1f-8222172d6a8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fw-bold mb-4</value>
      <webElementGuid>c8fb627a-04b2-415a-9f65-6da94a923c03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Telusuri Kategori</value>
      <webElementGuid>90d7bd5d-9b1b-4d5c-b19e-9ef60483b886</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container&quot;]/h5[@class=&quot;fw-bold mb-4&quot;]</value>
      <webElementGuid>65b43240-22e7-4ca2-8e2d-e9a08605ac4b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskon Hingga'])[1]/following::h5[2]</value>
      <webElementGuid>ed20636c-c187-4d8e-bbde-612571bf601c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulan RamadhanBanyak diskon!'])[1]/following::h5[2]</value>
      <webElementGuid>45ad757b-9ef0-46ec-b50b-b9993f4fa694</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[2]/h5</value>
      <webElementGuid>bbcfd9fe-e305-4bfb-9dc5-1e200b304dda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Telusuri Kategori' or . = 'Telusuri Kategori')]</value>
      <webElementGuid>33305d20-28bb-408e-a98e-ebd4c8413b53</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
