<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_HargaProduk</name>
   <tag></tag>
   <elementGuidId>38120815-5cb9-4a09-afa2-d1bfb032a354</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Dress Mewah'])[1]/following::p[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Rp 5.000.000' or . = 'Rp 5.000.000')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.card-text.fs-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>d08018cd-8ad1-4335-aad3-93a57fdc60fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-text fs-5</value>
      <webElementGuid>873ea9b4-bc71-4078-9879-e46758a74530</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Rp 5.000.000</value>
      <webElementGuid>7d60d540-9785-434a-9fa6-d13d3afb1cc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]/p[@class=&quot;card-text fs-5&quot;]</value>
      <webElementGuid>e75a2648-0a8c-4871-91de-b98cb1e71db5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dress Mewah'])[1]/following::p[2]</value>
      <webElementGuid>9fff6e37-a6f0-4d42-ad49-f474c5b8e9a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::p[3]</value>
      <webElementGuid>26a0dcf5-77ac-4bbe-96a5-6272e39e043b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/preceding::p[1]</value>
      <webElementGuid>5e3783bb-dfa7-459d-8450-84232767072f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lenny Octavia'])[1]/preceding::p[1]</value>
      <webElementGuid>6c8fcd23-1745-4b34-a811-63050415440f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Rp 5.000.000']/parent::*</value>
      <webElementGuid>49194208-daa1-412b-b1f5-e357ff3d29d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[2]</value>
      <webElementGuid>fb0c6458-cec2-4fa6-a988-a762848ef1de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Rp 5.000.000' or . = 'Rp 5.000.000')]</value>
      <webElementGuid>2197541c-a8cc-4ae0-963c-6a206046428a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
