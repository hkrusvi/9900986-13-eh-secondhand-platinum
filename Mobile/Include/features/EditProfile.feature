#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@EditProfile
Feature: Edit Profile
  User wants to edit profile

  @EDP001
  Scenario: EDP001 - Users want to change information on their profile with complete data
    Given User already login to account
    Then User click Edit Profile icon
    When Users will see edit profile form 
    And User click and insert Name
    And User click and insert Mobile Number
    And User click and insert City Name
    And User click and insert Address
    Then User will see the profile that has been successfully changed 
    
  @EDP002
  Scenario: EDP002 - User wants to change profile but data is incomplete
    Given User already login to account
    Then User click Edit Profile icon
    When Users will see edit profile form 
    And User clicks on the username form but does not fill in the form
    Then A notification will appear that the form must be filled in
    
  @EDP003
  Scenario: EDP003 - Users fill in the cellphone number column with letters
    Given User already login to account
    Then User click Edit Profile icon
    When Users will see edit profile form 
    And Users fill in the cellphone number column with letters
    Then User will see the profile that has been successfully changed 
    
  @EDP001
  Scenario: EDP004 - User fills in the name column with numbers
    Given User already login to account
    Then User click Edit Profile icon
    When Users will see edit profile form 
    And User fills in the name column with numbers
    Then User will see the profile that has been successfully changed 
    

   