<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Terbitkan</name>
   <tag></tag>
   <elementGuidId>bc9a748d-7977-4aa2-a7b8-c78e8f166194</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'product_status_published']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_status_published</value>
      <webElementGuid>8e9eac67-a454-43c0-88ee-c0e23af9fec4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
