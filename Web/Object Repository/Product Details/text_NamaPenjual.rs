<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_NamaPenjual</name>
   <tag></tag>
   <elementGuidId>a5746837-b099-4b99-886d-1853ac829f3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Lenny Octavia' or . = 'Lenny Octavia')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card.py-2.px-4.rounded-4.shadow.border-0.mt-5 > div.d-flex.align-items-center > div.card-body > div.fs-5.fw-bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4a7513fa-f134-4278-a7dc-07addae93d9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-5 fw-bold</value>
      <webElementGuid>16538684-41ff-4c9f-a1ed-69d02f56c0df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lenny Octavia</value>
      <webElementGuid>ddd65596-21d8-4c20-93f9-5358a00ffd1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card py-2 px-4 rounded-4 shadow border-0 mt-5&quot;]/div[@class=&quot;d-flex align-items-center&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;fs-5 fw-bold&quot;]</value>
      <webElementGuid>aec85713-f4cb-4f7b-8949-945942445ef9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/following::div[4]</value>
      <webElementGuid>eab5f1ce-56ea-4dcb-8f32-ad151b848543</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dress Mewah'])[1]/following::div[4]</value>
      <webElementGuid>80021e9b-bc0e-4192-aca5-25edbc5e37aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Solo'])[1]/preceding::div[1]</value>
      <webElementGuid>4f6376ee-a0db-4958-97a3-fda7c3531c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/preceding::div[3]</value>
      <webElementGuid>bb84901e-0ef0-4269-94bc-623cd24c8760</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lenny Octavia']/parent::*</value>
      <webElementGuid>f40daac7-6a91-4444-b469-a0af6342ffbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div</value>
      <webElementGuid>fd111a3a-85c2-491d-afa1-01f78d79abbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Lenny Octavia' or . = 'Lenny Octavia')]</value>
      <webElementGuid>23c8d0db-7a2a-4732-a3e5-efefa4576bcc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
