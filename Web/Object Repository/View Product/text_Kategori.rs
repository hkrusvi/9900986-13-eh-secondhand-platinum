<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Kategori</name>
   <tag></tag>
   <elementGuidId>c1f8bb75-9cc3-457b-906e-4aefb60d6daa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::h5[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h5.card-title.mb-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>9f36ce8f-78c2-4702-9973-d74cafe00a5c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title mb-4</value>
      <webElementGuid>3f6aa335-5bb7-4243-bfe8-4f32a438b55e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kategori</value>
      <webElementGuid>d78283d1-d4da-40a8-a780-e608e2b40f33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-3&quot;]/div[@class=&quot;card border-0 rounded-4 shadow p-4&quot;]/h5[@class=&quot;card-title mb-4&quot;]</value>
      <webElementGuid>1c512403-cebc-4cbd-aafe-286536246675</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::h5[1]</value>
      <webElementGuid>23595c64-6928-4174-8965-e8bf73f62bdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[2]/following::h5[1]</value>
      <webElementGuid>b866e402-7ac7-4037-9870-c5ca408002b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Produk'])[1]/preceding::h5[1]</value>
      <webElementGuid>fa740b95-953d-428f-8d0c-d58a0c555e52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/preceding::h5[1]</value>
      <webElementGuid>6fb08d1c-99c8-45dd-a8e7-df1a7d681e1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kategori']/parent::*</value>
      <webElementGuid>f30ac394-8422-4299-87f4-69540d448c1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5</value>
      <webElementGuid>f593423b-16f8-43e5-9ad4-92fdca1f5485</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'Kategori' or . = 'Kategori')]</value>
      <webElementGuid>ee6dc331-57a5-42cf-abbc-e4ead0116d2c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
