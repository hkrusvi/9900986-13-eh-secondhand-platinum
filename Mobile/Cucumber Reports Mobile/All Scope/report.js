$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/AddProduct.feature");
formatter.feature({
  "name": "Add Product",
  "description": "  User wants to add products",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@AddProduct"
    }
  ]
});
formatter.scenario({
  "name": "ADP001 - Users fill in the product name with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP002 - User did not fill in the product name column",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert a product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_not_insert_a_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP003 - Users input product descriptions with long explanations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users input product descriptions with long explanations",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.users_input_product_descriptions_with_long_explanations()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP004 - User does not fill in the product price column",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP004"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert product price column",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_not_insert_product_price_column()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/EditProfile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "  User wants to edit profile",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@EditProfile"
    }
  ]
});
formatter.scenario({
  "name": "EDP001 - Users want to change information on their profile with complete data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Name",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Mobile Number",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Mobile_Number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert City Name",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_City_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Address",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP002 - User wants to change profile but data is incomplete",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the username form but does not fill in the form",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_clicks_on_the_username_form_but_does_not_fill_in_the_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A notification will appear that the form must be filled in",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.a_notification_will_appear_that_the_form_must_be_filled_in()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP003 - Users fill in the cellphone number column with letters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users fill in the cellphone number column with letters",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.users_fill_in_the_cellphone_number_column_with_letters()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP004 - User fills in the name column with numbers",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills in the name column with numbers",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_fills_in_the_name_column_with_numbers()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "\tUser want to login to SecondHand app",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Login"
    }
  ]
});
formatter.scenario({
  "name": "LGI001 - User want to login using valid credentials.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI001"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_logged_in_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see AKUN SAYA screen",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_AKUN_SAYA_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see email match with inputted email",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_email_match_with_inputted_email()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI002 - User want to login using empty email.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI002"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Email tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI003 - User want to login using invalid email format.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI003"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input invalid email format \"second$hand.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_invalid_email_format(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Email tidak valid\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI004 - User want to login using unregistered email.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI004"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input unregistered email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_unregistered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error toast \"Email atau kata sandi salah\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI005 - User want to login using empty password.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI005"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Password tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI006 - User want to login using unregistered password.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI006"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input unregistered password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_unregistered_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error toast \"Email atau kata sandi salah\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI007 - User want to navigate to Register screen from Login screen.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI007"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR link",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_tap_on_DAFTAR_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see DAFTAR screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see DAFTAR form",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_DAFTAR_form()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Register.feature");
formatter.feature({
  "name": "Register",
  "description": "\tUser want to register to SecondHand app",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Register"
    }
  ]
});
formatter.scenario({
  "name": "REG001 - User want to register using valid credentials",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG001"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User registered successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_registered_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see AKUN SAYA screen",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_AKUN_SAYA_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see email match with inputted email",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_email_match_with_inputted_email()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG002 - User want to register while leaving Nama field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG002"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Nama tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG003 - User want to register while leaving email empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG003"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Email tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG004 - User want register while providing already registered email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG004"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input registered email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_registered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error toast \"Email sudah digunakan\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG005 - User want to register while leaving password empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG005"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Password tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG006 - User want to register with password less than six characters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG006"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password less than six",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password_less_than_six()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Password harus lebih dari 6 karakter\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/ViewProduct.feature");
formatter.feature({
  "name": "View Product",
  "description": "  Users want to see products that have been published",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@ViewProduct"
    }
  ]
});
formatter.scenario({
  "name": "VPR001 - Users want to see all products that have been published",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see products that have been successfully published",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.users_will_see_products_that_have_been_successfully_published()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR002 - Users want to see product data that buyers are interested in",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users click Diminati Page",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.users_click_Diminati_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see a list of products that buyers are interested in",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_will_see_a_list_of_products_that_buyers_are_interested_in()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR003 - Users want to see data on products that have been sold",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terjual Page and User will see a list of products that have been successfully sold",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terjual_Page_and_User_will_see_a_list_of_products_that_have_been_successfully_sold()"
});
formatter.result({
  "status": "passed"
});
});