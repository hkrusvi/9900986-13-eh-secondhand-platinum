#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Login
Feature: Login
	User want to login to SecondHand app
	
	@LGI001
	Scenario: LGI001 - User want to login using valid credentials. 
		Given User already on MASUK screen
		When User input correct email
		And User input correct password
		And User tap on MASUK button
		Then User logged in successfully
		And User see AKUN SAYA screen
		And User see email match with inputted email
		
	@LGI002
	Scenario: LGI002 - User want to login using empty email. 
		Given User already on MASUK screen
		When User input correct password
		And User tap on MASUK button
		Then User see login error messages "Email tidak boleh kosong"
		And User can not log in
		And User stay on screen "Masuk"
		
	@LGI003
	Scenario: LGI003 - User want to login using invalid email format. 
		Given User already on MASUK screen
		When User input invalid email format "second$hand.com"
		And User input correct password
		And User tap on MASUK button
		Then User see login error messages "Email tidak valid"
		And User can not log in
		And User stay on screen "Masuk"
	
	@LGI004
	Scenario: LGI004 - User want to login using unregistered email. 
		Given User already on MASUK screen
		When User input unregistered email
		And User input correct password
		And User tap on MASUK button
		Then User see login error toast "Email atau kata sandi salah"
		And User can not log in
		And User stay on screen "Masuk"
		
	@LGI005
	Scenario: LGI005 - User want to login using empty password. 
		Given User already on MASUK screen
		When User input correct email
		And User tap on MASUK button
		Then User see login error messages "Password tidak boleh kosong"
		And User can not log in
		And User stay on screen "Masuk"
	
	@LGI006
	Scenario: LGI006 - User want to login using unregistered password. 
		Given User already on MASUK screen
		When User input correct email
		And User input unregistered password
		And User tap on MASUK button
		Then User see login error toast "Email atau kata sandi salah"
		And User can not log in
		And User stay on screen "Masuk"
	
	@LGI007
	Scenario: LGI007 - User want to navigate to Register screen from Login screen. 
		Given User already on MASUK screen
		When User tap on DAFTAR link
		Then User see DAFTAR screen
		And User see DAFTAR form