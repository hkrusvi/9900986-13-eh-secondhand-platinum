<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_KategoriProduk</name>
   <tag></tag>
   <elementGuidId>ab82a6ce-75d7-40c1-be3e-dc0467045733</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Dress Mewah'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Baju' or . = 'Baju')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card.p-2.rounded-4.shadow.border-0 > div.card-body > p.card-text.text-black-50.fs-6</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>07d497c2-a2c4-4786-9ae8-0ecf48026659</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-text text-black-50 fs-6</value>
      <webElementGuid>80863160-c106-4b15-b32e-860d47b3eb4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Baju</value>
      <webElementGuid>1e9ee616-9379-439a-beab-9e2309feae19</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container my-5&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-4&quot;]/div[@class=&quot;card p-2 rounded-4 shadow border-0&quot;]/div[@class=&quot;card-body&quot;]/p[@class=&quot;card-text text-black-50 fs-6&quot;]</value>
      <webElementGuid>28117cb8-157c-4f96-8880-ae582d0e20c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dress Mewah'])[1]/following::p[1]</value>
      <webElementGuid>6a33b8e4-5325-4001-8291-74a37ee5acff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/following::p[2]</value>
      <webElementGuid>b1f5e817-e91b-43c6-9709-3481dcffbfc6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saya tertarik dan ingin nego'])[1]/preceding::p[2]</value>
      <webElementGuid>36132a68-df7e-4942-ab53-7578187135ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lenny Octavia'])[1]/preceding::p[2]</value>
      <webElementGuid>eaf94dcf-ce1a-40db-bc20-75b50cfa35f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Baju']/parent::*</value>
      <webElementGuid>5fbd46cb-f63b-4fd7-8889-1c9fa448fc85</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/p</value>
      <webElementGuid>e500b5a0-93d8-491e-817a-6d1a10ca48b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Baju' or . = 'Baju')]</value>
      <webElementGuid>9a52c80f-e64e-4987-ad35-3598ea3f5e23</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
