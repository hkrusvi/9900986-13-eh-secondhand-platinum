package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

public class AddProduct {
	@Then("User click beranda page")
	public void user_click_beranda_page() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Beranda'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click add product with + button")
	public void user_click_add_product_with_button() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Click Add Product at Home'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User will see add product form")
	public void user_will_see_add_product_form() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Verify Content Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User insert product name")
	public void user_insert_product_name() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Product Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User insert product price")
	public void user_insert_product_price() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Product Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User choose the category")
	public void user_choose_the_category() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Choose Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("insert seller location")
	public void insert_seller_location() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Location'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User insert product description")
	public void user_insert_product_description() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User insert product image")
	public void user_insert_product_image() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Add Product Image'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Terbitkan")
	public void user_click_Terbitkan() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Click Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		//		Mobile.pressBack()
		//		WebUI.callTestCase(findTestCase('Pages/Home/Click Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("User not insert a product name")
	public void user_not_insert_a_product_name() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Empty Product Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Users input product descriptions with long explanations")
	public void users_input_product_descriptions_with_long_explanations() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Long Description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User not insert product price column")
	public void user_not_insert_product_price_column() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Empty Product Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
