import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class KeywordList {

	@Keyword
	def randomString(def length) {
		def randomWord = new java.util.Random().with { r ->
			def pool = ('a'..'z') + ('A'..'Z') + (0..9)
			(1..length).collect { pool[r.nextInt(pool.size())] }.join('')
		}
		return randomWord
	}
	
	def randomNumber(def length) {
		def randomWord = new java.util.Random().with { r ->
			def pool = (0..9)
			(1..length).collect { pool[r.nextInt(pool.size())] }.join('')
		}
		return randomWord
	}

	@Keyword
	def randomEmail() {
		def emailAddress = randomString(7) + '@mail.com'
		return emailAddress
	}
	
	@Keyword
	def randomMobilePhone() {
		def phoneNumber = '08' + randomNumber(10)
		return phoneNumber
	}
}
