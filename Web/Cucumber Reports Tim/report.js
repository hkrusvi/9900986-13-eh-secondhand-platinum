$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/AddProduct.feature");
formatter.feature({
  "name": "Add Product",
  "description": "  User wants to add a new product",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@AddProduct"
    }
  ]
});
formatter.scenario({
  "name": "ADP001 - Users fill in the product name with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP001"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see button Jual",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button Jual",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see product detail textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_product_detail_textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Name",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Price",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Choose Product Category",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_Choose_Product_Category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Description",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Image",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP002 - User did not fill in the product name column",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP002"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see button Jual",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button Jual",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see product detail textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_product_detail_textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert in the product name",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_not_insert_in_the_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Price",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Choose Product Category",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_Choose_Product_Category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Description",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Image",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP003 - User inputs the price column with letters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP003"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see button Jual",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button Jual",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see product detail textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_product_detail_textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Name",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Price with letters",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_Product_Price_with_letters()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Choose Product Category",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_Choose_Product_Category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Description",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Image",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP004 - Users fill in the product name with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP004"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see button Jual",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click button Jual",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_button_Jual()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see product detail textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_product_detail_textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Name",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert Product Price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_not_insert_Product_Price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Choose Product Category",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_Choose_Product_Category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Description",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Image",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/EditProfile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "  User want to edit their profile",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@EditProfile"
    }
  ]
});
formatter.scenario({
  "name": "EDP001 - Users want to change information on their profile with complete data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP001"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see and click Profile icon",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.user_will_see_and_click_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click the profile",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_the_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see Edit Profile Textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_Edit_Profile_Textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click and insert new profile photo",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_and_insert_new_profile_photo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert new name \"Hendri\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_new_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose new city \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_choose_new_city(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Address \"Jalan Kenangan No.30\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_Address(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input mobile number",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_mobile_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP002 - User wants to change profile but data is incomplete",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP002"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see and click Profile icon",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.user_will_see_and_click_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click the profile",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_the_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see Edit Profile Textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_Edit_Profile_Textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click and insert new profile photo",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_and_insert_new_profile_photo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user did not fill in the name field",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.the_user_did_not_fill_in_the_name_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose new city \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_choose_new_city(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Address \"Jalan Kenangan No.30\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_Address(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input mobile number",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_mobile_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP003 - Users fill in the cellphone number column with letters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP003"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see and click Profile icon",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.user_will_see_and_click_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click the profile",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_the_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see Edit Profile Textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_Edit_Profile_Textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click and insert new profile photo",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_and_insert_new_profile_photo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert new name \"Hendri\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_new_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose new city \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_choose_new_city(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Address \"Jalan Kenangan No.30\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_Address(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters letters in the cellphone number column \"hehehehe\"",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_enters_letters_in_the_cellphone_number_column(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP004 - Users want to change information on their profile with complete data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP004"
    }
  ]
});
formatter.step({
  "name": "User Already landing on Homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_Already_landing_on_Homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see and click Profile icon",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.user_will_see_and_click_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click the profile",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_the_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see Edit Profile Textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_Edit_Profile_Textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click and insert new profile photo",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_and_insert_new_profile_photo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters number in the name column \"123456789\"",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_enters_number_in_the_name_column(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose new city \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_choose_new_city(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Address \"Jalan Kenangan No.30\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_Address(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input mobile number",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_mobile_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "\tUser want to login to SecondHand website",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Login"
    }
  ]
});
formatter.scenario({
  "name": "LGI001 - User want to login using valid credentials.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI001"
    }
  ]
});
formatter.step({
  "name": "User already on LOGIN page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_click_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_logged_in_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Daftar Jual Saya icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Daftar_Jual_Saya_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Notification icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Notification_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Akun icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Akun_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI002 - User want to login using invalid email.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI002"
    }
  ]
});
formatter.step({
  "name": "User already on LOGIN page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input invalid email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_invalid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_click_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Invalid Email or password.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on LOGIN page",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI003 - User want to login with leaving Email field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI003"
    }
  ]
});
formatter.step({
  "name": "User already on LOGIN page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_click_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on LOGIN page",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI004 - User want to login with leaving Password field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI004"
    }
  ]
});
formatter.step({
  "name": "User already on LOGIN page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_click_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on LOGIN page",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI005 - User want to navigate from Login page to Register page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI005"
    }
  ]
});
formatter.step({
  "name": "User already on LOGIN page",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar link",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_click_Daftar_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see REGISTER page",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see REGISTER form",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_REGISTER_form()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Register.feature");
formatter.feature({
  "name": "Register",
  "description": "\tUser want to register to SecondHand website",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Register"
    }
  ]
});
formatter.scenario({
  "name": "REG001 - User want to register using valid credentials",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG001"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_click_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User registered successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_registered_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Daftar Jual Saya icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Daftar_Jual_Saya_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Notification icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Notification_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see Akun icon",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_Akun_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG002 - User want to register with leaving Name field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG002"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_click_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on REGISTER page",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG003 - User want to register with leaving Email field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG003"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_click_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on REGISTER page",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG004 - User want to register using already registered email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG004"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input already registered email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_already_registered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_click_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see error messages \"Email has already been taken\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on REGISTER page",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG005 - User want to register with leaving Password field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG005"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_click_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on REGISTER page",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG006 - User want to navigate from Register Page to Login Page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG006"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER page",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Masuk link",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_click_Masuk_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see LOGIN page",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_LOGIN_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see LOGIN form",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_see_LOGIN_form()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/ViewProduct.feature");
formatter.feature({
  "name": "View Product",
  "description": "  User want to use several features on daftar jual saya page",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@ViewProduct"
    }
  ]
});
formatter.scenario({
  "name": "VPR001 - Users want to navigate to edit profile page from Daftar Jual Saya Page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR001"
    }
  ]
});
formatter.step({
  "name": "User already landing on Daftar Jual Saya page",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_landing_on_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Edit Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Edit_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see Edit Profile Textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_Edit_Profile_Textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click and insert new profile photo",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_and_insert_new_profile_photo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert new name \"Hendri\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_new_name(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose new city \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_choose_new_city(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Address \"Jalan Kenangan No.30\"",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_Address(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input mobile number",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_input_mobile_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click Simpan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_Click_Simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR002 - Users want to add new products via \"Daftar Jual Saya\" page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR002"
    }
  ]
});
formatter.step({
  "name": "User already landing on Daftar Jual Saya page",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_landing_on_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Tambah produk button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Tambah_produk_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see product detail textfield",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_will_see_product_detail_textfield()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Name",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Price",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Choose Product Category",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_Choose_Product_Category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Description",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert Product Image",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_insert_Product_Image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan button",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR003 - Users want to see a list of products that buyers are interested in",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR003"
    }
  ]
});
formatter.step({
  "name": "User already landing on Daftar Jual Saya page",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_landing_on_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Diminati button",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Diminati_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see a list of products that buyers are interested in",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_will_see_a_list_of_products_that_buyers_are_interested_in()"
});
formatter.result({
  "status": "passed"
});
});