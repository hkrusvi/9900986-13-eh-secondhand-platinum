package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class EditProfile {
	@Then("User click Edit Profile icon")
	public void user_click_Edit_Profile_icon() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Click Edit Profile Icon'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Users will see edit profile form")
	public void users_will_see_edit_profile_form() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Verify content field'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click and insert Name")
	public void user_click_and_insert_Name() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click and insert Mobile Number")
	public void user_click_and_insert_Mobile_Number() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Mobile Number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click and insert City Name")
	public void user_click_and_insert_City_Name() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit City'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click and insert Address")
	public void user_click_and_insert_Address() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Address'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will see the profile that has been successfully changed")
	public void user_will_see_the_profile_that_has_been_successfully_changed() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Verify content field'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User clicks on the username form but does not fill in the form")
	public void user_clicks_on_the_username_form_but_does_not_fill_in_the_form() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Empty Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("A notification will appear that the form must be filled in")
	public void a_notification_will_appear_that_the_form_must_be_filled_in() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Notif Nama Wajib diisi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Users fill in the cellphone number column with letters")
	public void users_fill_in_the_cellphone_number_column_with_letters() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Wrong Mobile Number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User fills in the name column with numbers")
	public void user_fills_in_the_name_column_with_numbers() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Wrong Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
