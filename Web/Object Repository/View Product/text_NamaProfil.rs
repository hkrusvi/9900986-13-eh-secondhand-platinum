<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_NamaProfil</name>
   <tag></tag>
   <elementGuidId>3d8b25e0-f024-4be8-9b94-82cc53c6b043</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Jual Saya'])[1]/following::div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.py-3.ps-4 > div.fs-5.fw-bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>aafac850-f66c-4589-893b-1ff67dd66955</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-5 fw-bold</value>
      <webElementGuid>8ce75a13-ee0f-4605-9163-887f0b8a5240</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>51651</value>
      <webElementGuid>e00644c6-252d-4e1e-b30c-e0ee1fcf6cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;card py-2 px-4 rounded-4 border-4 border-light mt-5 d-flex justify-content-between&quot;]/div[@class=&quot;d-flex align-items-center&quot;]/div[@class=&quot;py-3 ps-4&quot;]/div[@class=&quot;fs-5 fw-bold&quot;]</value>
      <webElementGuid>71e46a67-8026-4588-85b6-8dacf481bf0e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Jual Saya'])[1]/following::div[4]</value>
      <webElementGuid>79719190-420c-405b-b4d2-6a8d66cb0362</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jogja'])[2]/preceding::div[1]</value>
      <webElementGuid>ad331346-3785-4bd6-8e44-055d72de95d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/preceding::div[2]</value>
      <webElementGuid>56911012-259f-45fd-85e3-61688ea3cd55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div</value>
      <webElementGuid>7acefd8f-59a4-4d4b-abea-63341acd9082</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '51651' or . = '51651')]</value>
      <webElementGuid>20003789-6c24-4503-83e3-ec37efcfcf4a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
