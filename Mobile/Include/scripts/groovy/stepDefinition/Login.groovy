package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Login {

	@Given("User already on MASUK screen")
	public void user_already_on_MASUK_screen() {
		Mobile.callTestCase(findTestCase('Pages/Login/Already On Login Screen'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input correct email")
	public void user_input_correct_email() {
		Mobile.callTestCase(findTestCase('Pages/Login/Input Email'), [('email') : GlobalVariable.email], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input correct password")
	public void user_input_correct_password() {
		Mobile.callTestCase(findTestCase('Pages/Login/Input Password'), [('password') : GlobalVariable.password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap on MASUK button")
	public void user_tap_on_MASUK_button() {
		Mobile.callTestCase(findTestCase('Pages/Login/Tap Masuk Button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User logged in successfully")
	public void user_logged_in_successfully() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Session Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see AKUN SAYA screen")
	public void user_see_AKUN_SAYA_screen() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Akun Saya Screen Title'), ['expectedTitle': 'Akun Saya'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see email match with inputted email")
	public void user_see_email_match_with_inputted_email() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Account Information'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Then("User see login error messages {string}")
	public void user_see_login_error_messages(String error) {
		Mobile.callTestCase(findTestCase('Pages/Login/Verify Error Messages'), ['expectedError': error], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see login error toast {string}")
	public void user_see_login_error_toast(String error) {
		Mobile.callTestCase(findTestCase('Pages/Login/Verify Error Toast'), ['expectedError': error], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can not log in")
	public void user_can_not_log_in() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Session Fail'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User stay on screen {string}")
	public void user_stay_on_screen(String title) {
		Mobile.callTestCase(findTestCase('Pages/Login/Verify Masuk Screen Title'), ['expectedTitle': title], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("User input invalid email format {string}")
	public void user_input_invalid_email_format(String email) {
		Mobile.callTestCase(findTestCase('Pages/Login/Input Email'), ['email': email], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input unregistered email")
	public void user_input_unregistered_email() {
		Mobile.callTestCase(findTestCase('Pages/Login/Input Unregistered Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input unregistered password")
	public void user_input_unregistered_password() {
		Mobile.callTestCase(findTestCase('Pages/Login/Input Unregistered Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap on DAFTAR link")
	public void user_tap_on_DAFTAR_link() {
		Mobile.callTestCase(findTestCase('Pages/Login/Tap Daftar Link'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see DAFTAR screen")
	public void user_see_DAFTAR_screen() {
		Mobile.callTestCase(findTestCase('Pages/Register/Verify Daftar Screen Title'), ['expectedTitle': 'Daftar'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see DAFTAR form")
	public void user_see_DAFTAR_form() {
		Mobile.callTestCase(findTestCase('Pages/Register/Verify Daftar Form'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}
}
