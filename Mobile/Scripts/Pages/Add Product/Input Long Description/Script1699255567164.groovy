import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementVisible(findTestObject('Add Product/Textarea Deskripsi'), 0)

Mobile.tap(findTestObject('Add Product/Textarea Deskripsi'), 0)

Mobile.setText(findTestObject('Add Product/Textarea Deskripsi'), 'Mobil listrik Honda e:Ny1 sudah dijual di pasar China dan Eropa. SUV ramah lingkungan yang mirip sekali dengan Honda HR-V itu kapan akan dijual di Indonesia? Bicara pasar Indonesia, Honda mengaku telat dalam adaptasi kendaraan listrik. Saat ini Honda Prospect Motor (HPM) baru memiliki satu unit elektrifikasi yang dijual umum, yakni Honda CR-V Hybrid, yang diperkenalkan dan mulai dijual pada Agustus 2023. Sementara di pasar global, produk-produk elektrifikasi Honda saat ini juga belum bisa dibilang banyak. Pada ajang Japan Mobility Show 2023 lalu, Honda memperkenalkan SUV listrik Prologue, yang rencananya akan mulai dijual di Amerika Utara pada awal 2024.', 
    0)

Mobile.pressBack()

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

