#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Login
Feature: Login
	User want to login to SecondHand website
	
	@LGI001
	Scenario: LGI001 - User want to login using valid credentials. 
		Given User already on LOGIN page
		When User input correct email
		And User input correct password
		And User click on MASUK button
		Then User logged in successfully
		And User see Daftar Jual Saya icon
		And User see Notification icon
		And User see Akun icon
		
	@LGI002
	Scenario: LGI002 - User want to login using invalid email. 
		Given User already on LOGIN page
		When User input invalid email
		And User input correct password
		And User click on MASUK button
		Then User see login error messages "Invalid Email or password."
		And User can not log in
		And User stay on LOGIN page
		
	@LGI003
	Scenario: LGI003 - User want to login with leaving Email field empty 
		Given User already on LOGIN page
		When User input correct password
		And User click on MASUK button
		Then User can not log in
		And User stay on LOGIN page
		
	@LGI004
	Scenario: LGI004 - User want to login with leaving Password field empty 
		Given User already on LOGIN page
		When User input correct email
		And User click on MASUK button
		Then User can not log in
		And User stay on LOGIN page
		
	@LGI005
	Scenario: LGI005 - User want to navigate from Login page to Register page 
		Given User already on LOGIN page
		When User click Daftar link
		Then User see REGISTER page
		And User see REGISTER form