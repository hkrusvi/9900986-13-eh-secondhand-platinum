#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@AddProduct
Feature: Add Product
  User wants to add a new product

  @ADP001
  Scenario: ADP001 - Users fill in the product name with valid data
  	Given User Already landing on Homepage
  	When User will see button Jual
  	Then User click button Jual
 	  When User will see product detail textfield
 	  Then User insert Product Name
    And User insert Product Price
    And User Choose Product Category
    And User insert Product Description
    And User insert Product Image
    Then User click Terbitkan button
    
  @ADP002
  Scenario: ADP002 - User did not fill in the product name column
  	Given User Already landing on Homepage
  	When User will see button Jual
  	Then User click button Jual
 	  When User will see product detail textfield
 	  Then User not insert in the product name
    And User insert Product Price
    And User Choose Product Category
    And User insert Product Description
    And User insert Product Image
    Then User click Terbitkan button
    
  @ADP003
  Scenario: ADP003 - User inputs the price column with letters
  	Given User Already landing on Homepage
  	When User will see button Jual
  	Then User click button Jual
 	  When User will see product detail textfield
 	  Then User insert Product Name
    And User insert Product Price with letters
    And User Choose Product Category
    And User insert Product Description
    And User insert Product Image
    Then User click Terbitkan button
    
  @ADP004
  Scenario: ADP004 - Users fill in the product name with valid data
  	Given User Already landing on Homepage
  	When User will see button Jual
  	Then User click button Jual
 	  When User will see product detail textfield
 	  Then User insert Product Name
    And User not insert Product Price
    And User Choose Product Category
    And User insert Product Description
    And User insert Product Image
    Then User click Terbitkan button  	
  
    
    