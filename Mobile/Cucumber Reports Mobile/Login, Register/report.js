$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Login.feature");
formatter.feature({
  "name": "Login",
  "description": "\tUser want to login to SecondHand app",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Login"
    }
  ]
});
formatter.scenario({
  "name": "LGI001 - User want to login using valid credentials.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI001"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User logged in successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_logged_in_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see AKUN SAYA screen",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_AKUN_SAYA_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see email match with inputted email",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_email_match_with_inputted_email()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI002 - User want to login using empty email.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI002"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Email tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI003 - User want to login using invalid email format.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI003"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input invalid email format \"second$hand.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_invalid_email_format(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Email tidak valid\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI004 - User want to login using unregistered email.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI004"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input unregistered email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_unregistered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_correct_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error toast \"Email atau kata sandi salah\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI005 - User want to login using empty password.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI005"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error messages \"Password tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI006 - User want to login using unregistered password.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI006"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input correct email",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_input_correct_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input unregistered password",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_input_unregistered_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on MASUK button",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_tap_on_MASUK_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see login error toast \"Email atau kata sandi salah\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_login_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not log in",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_can_not_log_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on screen \"Masuk\"",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_stay_on_screen(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LGI007 - User want to navigate to Register screen from Login screen.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@LGI007"
    }
  ]
});
formatter.step({
  "name": "User already on MASUK screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Login.user_already_on_MASUK_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR link",
  "keyword": "When "
});
formatter.match({
  "location": "Login.user_tap_on_DAFTAR_link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see DAFTAR screen",
  "keyword": "Then "
});
formatter.match({
  "location": "Login.user_see_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see DAFTAR form",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_DAFTAR_form()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/Register.feature");
formatter.feature({
  "name": "Register",
  "description": "\tUser want to register to SecondHand app",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Register"
    }
  ]
});
formatter.scenario({
  "name": "REG001 - User want to register using valid credentials",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG001"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User registered successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_registered_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see AKUN SAYA screen",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_AKUN_SAYA_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see email match with inputted email",
  "keyword": "And "
});
formatter.match({
  "location": "Login.user_see_email_match_with_inputted_email()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG002 - User want to register while leaving Nama field empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG002"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Nama tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG003 - User want to register while leaving email empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG003"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Email tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG004 - User want register while providing already registered email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG004"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input registered email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_registered_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error toast \"Email sudah digunakan\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_toast(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG005 - User want to register while leaving password empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG005"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Password tidak boleh kosong\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG006 - User want to register with password less than six characters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@REG006"
    }
  ]
});
formatter.step({
  "name": "User already on REGISTER screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Register.user_already_on_REGISTER_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid name",
  "keyword": "When "
});
formatter.match({
  "location": "Register.user_input_valid_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid email",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid password less than six",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_password_less_than_six()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid phone number",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_phone_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid city",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_city()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input valid address",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_input_valid_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on DAFTAR button",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_tap_on_DAFTAR_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User see register error messages \"Password harus lebih dari 6 karakter\"",
  "keyword": "Then "
});
formatter.match({
  "location": "Register.user_see_register_error_messages(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can not register",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_can_not_register()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User stay on DAFTAR screen",
  "keyword": "And "
});
formatter.match({
  "location": "Register.user_stay_on_DAFTAR_screen()"
});
formatter.result({
  "status": "passed"
});
});