package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class ViewProduct {
	@Given("User already landing on Daftar Jual Saya page")
	public void user_already_landing_on_Daftar_Jual_Saya_page() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/User already landing on Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Click Edit Profile button")
	public void user_Click_Edit_Profile_button() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Click Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User will see Edit Profile Textfield")
	public void user_will_see_Edit_Profile_Textfield() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Verify Content Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Click and insert new profile photo")
	public void user_Click_and_insert_new_profile_photo() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Edit Photo Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert new name {string}")
	public void user_insert_new_name(String string) {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Input Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User choose new city {string}")
	public void user_choose_new_city(String string) {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Choose City'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input Address {string}")
	public void user_input_Address(String string) {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Input Address'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User input mobile number")
	public void user_input_mobile_number() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Input No Handphone'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Click Simpan button")
	public void user_Click_Simpan_button() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Click Save'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Tambah produk button")
	public void user_click_Tambah_produk_button() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Click Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User will see product detail textfield")
	public void user_will_see_product_detail_textfield() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Verify Content Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert Product Name")
	public void user_insert_Product_Name() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Product Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert Product Price")
	public void user_insert_Product_Price() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Product Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User Choose Product Category")
	public void user_Choose_Product_Category() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Choose Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert Product Description")
	public void user_insert_Product_Description() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Product Description'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert Product Image")
	public void user_insert_Product_Image() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Add Product Image'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click Terbitkan button")
	public void user_click_Terbitkan_button() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Click Terbitkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click Diminati button")
	public void user_click_Diminati_button() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Click Diminati'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will see a list of products that buyers are interested in")
	public void user_will_see_a_list_of_products_that_buyers_are_interested_in() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Verify Content Diminati'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
