<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Dropdown Spinner - Pilih Kategori</name>
   <tag></tag>
   <elementGuidId>b5c5d890-0452-4ef7-bb10-0bdc573e05da</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Spinner</value>
      <webElementGuid>fb69e0de-8046-47f0-9859-98701888f5a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>f5e7ead5-acbe-45cb-baf5-3f9bf79df395</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih Kategori</value>
      <webElementGuid>570aff3e-2c59-4419-aa78-5eeb7084cef6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand:id/et_product_category</value>
      <webElementGuid>12df68c7-284d-4b7e-afff-f582bfaa4203</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>id.binar.fp.secondhand</value>
      <webElementGuid>29328dbe-1220-4d26-a078-59ddd12b942b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>8ad19387-057f-44e5-a250-4525cee787c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>568e0dfe-72a4-4a3f-af05-1d2568032a60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>35de2c8f-9c1c-4f0f-87b9-2a8f66e6b492</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9715fb78-934b-4a74-abfd-085513f68a18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>8d9c28d6-1b13-4423-be4d-2bf0ac460e28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a1988dd0-6dcf-4d8a-848d-18f9a5073c13</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>1bae04c9-7b80-430e-8e61-10431c0765ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>0c8d97be-54c0-4560-b3ea-f5fe583b5dce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>73d5e654-3714-4d42-b295-f978f7c97bef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>b615ec26-9d2e-420a-b8ca-534628243a7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>44</value>
      <webElementGuid>b3dbfc15-0f78-4384-bd28-09de46d4ab2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>852</value>
      <webElementGuid>207a662b-94e1-4f46-95a9-c00be1b5657e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>992</value>
      <webElementGuid>efd7da9d-a6bd-45e0-a647-92955543d718</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>143</value>
      <webElementGuid>2026dd92-6693-45d2-afbc-21ee14b270b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[44,852][1036,995]</value>
      <webElementGuid>4b560540-9fcf-4eab-a84d-47d53854b915</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>786fcb30-7510-44e8-9f56-22a0296c199c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.widget.LinearLayout[3]/android.widget.FrameLayout[1]/android.widget.Spinner[1]</value>
      <webElementGuid>a2308360-a172-46ab-bc19-dd295a08bc9b</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.Spinner' and (@text = 'Pilih Kategori
Elektronik
Komputer dan Aksesoris
Handphone dan Aksesoris
Pakaian Pria
Sepatu Pria
Tas Pria
Aksesoris Fashion
Kesehatan
Hobi dan Koleksi
Makanan dan Minuman
Perawatan dan Kecantikan
Perlengkapan Rumah
Pakaian Wanita
Fashion Muslim
Fashion bayi dan Anak
Ibu dan Bayi
Sepatu Wanita
Tas Wanita
Otomotif
Olahraga dan Outdoor
Buku dan Alat Tulis
Voucher
Souvenir dan Pesta
Fotografi' or . = 'Pilih Kategori
Elektronik
Komputer dan Aksesoris
Handphone dan Aksesoris
Pakaian Pria
Sepatu Pria
Tas Pria
Aksesoris Fashion
Kesehatan
Hobi dan Koleksi
Makanan dan Minuman
Perawatan dan Kecantikan
Perlengkapan Rumah
Pakaian Wanita
Fashion Muslim
Fashion bayi dan Anak
Ibu dan Bayi
Sepatu Wanita
Tas Wanita
Otomotif
Olahraga dan Outdoor
Buku dan Alat Tulis
Voucher
Souvenir dan Pesta
Fotografi') and @resource-id = 'id.binar.fp.secondhand:id/et_product_category']</locator>
   <locatorStrategy>CUSTOM</locatorStrategy>
</MobileElementEntity>
