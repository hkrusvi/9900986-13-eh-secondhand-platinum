package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {

	@Given("User already on REGISTER screen")
	public void user_already_on_REGISTER_screen() {
		Mobile.callTestCase(findTestCase('Pages/Register/Already On Daftar Screen'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid name")
	public void user_input_valid_name() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid email")
	public void user_input_valid_email() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid password")
	public void user_input_valid_password() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Password'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid phone number")
	public void user_input_valid_phone_number() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input No Hp'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid city")
	public void user_input_valid_city() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Kota'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid address")
	public void user_input_valid_address() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Alamat'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap on DAFTAR button")
	public void user_tap_on_DAFTAR_button() {
		Mobile.callTestCase(findTestCase('Pages/Register/Tap Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User registered successfully")
	public void user_registered_successfully() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Session Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see register error messages {string}")
	public void user_see_register_error_messages(String error) {
		Mobile.callTestCase(findTestCase('Pages/Register/Verify Error Messages'), [ 'expectedError' : error ], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can not register")
	public void user_can_not_register() {
		Mobile.callTestCase(findTestCase('Pages/Akun Saya/Verify Session Fail'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User stay on DAFTAR screen")
	public void user_stay_on_DAFTAR_screen() {
		Mobile.callTestCase(findTestCase('Pages/Register/Verify Daftar Screen Title'), [ 'expectedTitle' : 'Daftar' ], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("User input registered email")
	public void user_input_registered_email() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Registered Email'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User see register error toast {string}")
	public void user_see_register_error_toast(String error) {
		Mobile.callTestCase(findTestCase('Pages/Register/Verify Error Toast'), [ 'expectedError' : error ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input valid password less than six")
	public void user_input_valid_password_less_than_six() {
		Mobile.callTestCase(findTestCase('Pages/Register/Input Password Less Than Six'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}