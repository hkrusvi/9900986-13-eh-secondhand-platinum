package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Login {
	@Given("User already on LOGIN page")
	public void user_already_on_LOGIN_page() {
		WebUI.callTestCase(findTestCase('Pages/Login/Already On Login Page'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input correct email")
	public void user_input_correct_email() {
		WebUI.callTestCase(findTestCase('Pages/Login/Input Registered Email'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input correct password")
	public void user_input_correct_password() {
		WebUI.callTestCase(findTestCase('Pages/Login/Input Password'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User click on MASUK button")
	public void user_click_on_MASUK_button() {
		WebUI.callTestCase(findTestCase('Pages/Login/Click Button Masuk'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User logged in successfully")
	public void user_logged_in_successfully() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Session Success'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input invalid email")
	public void user_input_invalid_email() {
		WebUI.callTestCase(findTestCase('Pages/Login/Input Random Email'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see login error messages {string}")
	public void user_see_login_error_messages(String errorMessages) {
		WebUI.callTestCase(findTestCase('Pages/Login/Verify Error Messages'), ['expectedErrorMessages': errorMessages], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User can not log in")
	public void user_can_not_log_in() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Session Fail'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User stay on LOGIN page")
	public void user_stay_on_LOGIN_page() {
		WebUI.callTestCase(findTestCase('Pages/Login/Verify Login Page Title'), ['expectedPageTitle':'Masuk'], FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.closeBrowser()
	}

	@When("User click Daftar link")
	public void user_click_Daftar_link() {
		WebUI.callTestCase(findTestCase('Pages/Login/Click Link Daftar'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see REGISTER page")
	public void user_see_REGISTER_page() {
		WebUI.callTestCase(findTestCase('Pages/Register/Verify Register Page Title'), ['expectedPageTitle':'Daftar'], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see REGISTER form")
	public void user_see_REGISTER_form() {
		WebUI.callTestCase(findTestCase('Pages/Register/Verify Register Form'), [:], FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.closeBrowser()
	}
}