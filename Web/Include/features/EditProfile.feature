#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@EditProfile
Feature: Edit Profile
  User want to edit their profile

  @EDP001
  Scenario: EDP001 - Users want to change information on their profile with complete data
  	Given User Already landing on Homepage
  	When User will see and click Profile icon
  	And User click the profile
  	When User will see Edit Profile Textfield
  	Then User Click and insert new profile photo
  	And User insert new name "Hendri"
    And User choose new city "Jogja"
    And User input Address "Jalan Kenangan No.30"
    And User input mobile number
    Then User Click Simpan button
    
  @EDP002
  Scenario: EDP002 - User wants to change profile but data is incomplete
  	Given User Already landing on Homepage
  	When User will see and click Profile icon
  	And User click the profile
  	When User will see Edit Profile Textfield
  	Then User Click and insert new profile photo
  	And The user did not fill in the name field
    And User choose new city "Jogja"
    And User input Address "Jalan Kenangan No.30"
    And User input mobile number
    Then User Click Simpan button
    
  @EDP003
  Scenario: EDP003 - Users fill in the cellphone number column with letters
  	Given User Already landing on Homepage
  	When User will see and click Profile icon
  	And User click the profile
  	When User will see Edit Profile Textfield
  	Then User Click and insert new profile photo
  	And User insert new name "Hendri"
    And User choose new city "Jogja"
    And User input Address "Jalan Kenangan No.30"
    And User enters letters in the cellphone number column "hehehehe"
    Then User Click Simpan button
    
  @EDP004
  Scenario: EDP004 - Users want to change information on their profile with complete data
  	Given User Already landing on Homepage
  	When User will see and click Profile icon
  	And User click the profile
  	When User will see Edit Profile Textfield
  	Then User Click and insert new profile photo
  	And User enters number in the name column "123456789"
    And User choose new city "Jogja"
    And User input Address "Jalan Kenangan No.30"
    And User input mobile number
    Then User Click Simpan button
   
    
    
    
    
 