#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Register
Feature: Register
	User want to register to SecondHand app
	
	@REG001
	Scenario: REG001 - User want to register using valid credentials 
		Given User already on REGISTER screen
		When User input valid name
		And User input valid email
		And User input valid password
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User registered successfully
		And User see AKUN SAYA screen
		And User see email match with inputted email
		
	@REG002
	Scenario: REG002 - User want to register while leaving Nama field empty 
		Given User already on REGISTER screen
		When User input valid email
		And User input valid password
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User see register error messages "Nama tidak boleh kosong"
		And User can not register
		And User stay on DAFTAR screen
		
	@REG003
	Scenario: REG003 - User want to register while leaving email empty 
		Given User already on REGISTER screen
		When User input valid name
		And User input valid password
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User see register error messages "Email tidak boleh kosong"
		And User can not register
		And User stay on DAFTAR screen	
	
	@REG004
	Scenario: REG004 - User want register while providing already registered email 
		Given User already on REGISTER screen
		When User input valid name
		And User input registered email
		And User input valid password
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User see register error toast "Email sudah digunakan"
		And User can not register
		And User stay on DAFTAR screen
		
	@REG005
	Scenario: REG005 - User want to register while leaving password empty 
		Given User already on REGISTER screen
		When User input valid name
		And User input valid email
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User see register error messages "Password tidak boleh kosong"
		And User can not register
		And User stay on DAFTAR screen
		
	@REG006
	Scenario: REG006 - User want to register with password less than six characters 
		Given User already on REGISTER screen
		When User input valid name
		And User input valid email
		And User input valid password less than six
		And User input valid phone number
		And User input valid city
		And User input valid address
		And User tap on DAFTAR button
		Then User see register error messages "Password harus lebih dari 6 karakter"
		And User can not register
		And User stay on DAFTAR screen	
