<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_SemuaProduk</name>
   <tag></tag>
   <elementGuidId>a5261ecc-79c0-445a-86fe-5b3db0426a35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.me-auto</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>81107e09-f2a4-4087-9cc7-5f8682431416</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>me-auto</value>
      <webElementGuid>db853095-cf07-442e-848b-6efe3a0e2bd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Semua Produk</value>
      <webElementGuid>1c871f3f-b929-4141-82b0-2ca15440e68f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-3&quot;]/div[@class=&quot;card border-0 rounded-4 shadow p-4&quot;]/ul[@class=&quot;nav flex-column&quot;]/li[@class=&quot;nav-item&quot;]/a[@class=&quot;nav-link d-flex py-3 border-bottom border-1&quot;]/span[@class=&quot;me-auto&quot;]</value>
      <webElementGuid>ac0046e1-665b-42ce-b662-2382470321a2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::span[1]</value>
      <webElementGuid>622185eb-140b-455a-89ac-f70dadda1c13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::span[1]</value>
      <webElementGuid>0416d079-ed40-486a-942f-d25ff853f06e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diminati'])[1]/preceding::span[1]</value>
      <webElementGuid>d734f889-8e1d-4279-83e3-9ea3ee85bef2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjual'])[1]/preceding::span[2]</value>
      <webElementGuid>c72b6502-b8ed-482d-b9ee-9d04df7cad36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Semua Produk']/parent::*</value>
      <webElementGuid>1e4c5556-5e04-4b84-9b20-fd3b09591159</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li/a/span</value>
      <webElementGuid>bcf67644-20c0-4785-bf0c-534eae85471e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Semua Produk' or . = 'Semua Produk')]</value>
      <webElementGuid>28b817d5-8c49-442a-a1d2-c7b49d0f4533</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
