<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon_ProdukSaya</name>
   <tag></tag>
   <elementGuidId>2053fadb-a568-4e2a-80a3-ae05be33c296</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.bi.bi-list-ul.me-4.me-lg-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li/a/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>3d280f26-50d2-4e36-b3e9-1319ef2108f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-list-ul me-4 me-lg-0</value>
      <webElementGuid>667c895a-c111-4a85-b27c-32c582f4904c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item me-0 me-lg-2 fs-5&quot;]/a[@class=&quot;nav-link d-flex align-items-center&quot;]/i[@class=&quot;bi bi-list-ul me-4 me-lg-0&quot;]</value>
      <webElementGuid>0b33cbca-e90b-49b2-8a0a-1672f7dc42bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li/a/i</value>
      <webElementGuid>89bdb576-938f-4497-b2ff-5cf68c239088</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/i</value>
      <webElementGuid>4286a1fc-3a5e-4120-9963-379252fecee7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
