package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class EditProfile {
	@Given("User Already landing on Homepage")
	public void user_Already_landing_on_Homepage() {
		WebUI.callTestCase(findTestCase('Pages/Home/User already landing on Homepage'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User will see and click Profile icon")
	public void user_will_see_and_click_Profile_icon() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click the profile")
	public void user_click_the_profile() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("The user did not fill in the name field")
	public void the_user_did_not_fill_in_the_name_field() {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/Empty Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User enters letters in the cellphone number column {string}")
	public void user_enters_letters_in_the_cellphone_number_column(String string) {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/input wrong data type handphone'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User enters number in the name column {string}")
	public void user_enters_number_in_the_name_column(String string) {
		WebUI.callTestCase(findTestCase('Pages/Edit Profile/input Wrong data type name'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
