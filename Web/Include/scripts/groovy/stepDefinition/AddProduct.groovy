package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


public class AddProduct {
	@When("User will see button Jual")
	public void user_will_see_button_Jual() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Verify Content button jual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click button Jual")
	public void user_click_button_Jual() {
		WebUI.callTestCase(findTestCase('Pages/Home/Click Jual button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User not insert in the product name")
	public void user_not_insert_in_the_product_name() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Empty Product Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User insert Product Price with letters")
	public void user_insert_Product_Price_with_letters() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Input Wrong Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User not insert Product Price")
	public void user_not_insert_Product_Price() {
		WebUI.callTestCase(findTestCase('Pages/Add Product/Empty Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}

