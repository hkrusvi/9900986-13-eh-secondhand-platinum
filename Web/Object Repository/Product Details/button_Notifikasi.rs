<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Notifikasi</name>
   <tag></tag>
   <elementGuidId>80d0a033-651c-431a-82f3-06c67481d386</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/a/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/a[@class=&quot;nav-link dropdown-toggle position-relative&quot;]/i[@class=&quot;bi bi-bell&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.dropdown-toggle.position-relative > i.bi.bi-bell</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>f1610322-4ae8-47ea-8afb-ffa0b2061732</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-bell</value>
      <webElementGuid>549b85fa-e42c-410a-b5c7-f03f50c17bd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown me-0 me-lg-2 fs-5 d-none d-xl-block position-relative&quot;]/a[@class=&quot;nav-link dropdown-toggle position-relative&quot;]/i[@class=&quot;bi bi-bell&quot;]</value>
      <webElementGuid>806747eb-011a-4eef-acc8-2cc058d61e48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[3]/a/i</value>
      <webElementGuid>d0dd9953-7753-4066-8aa3-5cde67b50455</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a/i</value>
      <webElementGuid>e1ae29c4-b0b1-4858-950c-48d294f08b81</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
