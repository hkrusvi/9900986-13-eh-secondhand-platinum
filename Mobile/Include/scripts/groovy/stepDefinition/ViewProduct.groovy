package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class ViewProduct {
	@Given("User already login to account")
	public void user_already_login_to_account() {
		WebUI.callTestCase(findTestCase('Pages/Home/User already login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click Daftar Jual Saya")
	public void user_click_Daftar_Jual_Saya() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Click Daftar Jual Saya'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Users will see products that have been successfully published")
	public void users_will_see_products_that_have_been_successfully_published() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Verify Content Published Product'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.pressBack()
		WebUI.callTestCase(findTestCase('Pages/Home/Click Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Then("Users click Diminati Page")
	public void users_click_Diminati_Page() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Click Diminati'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will see a list of products that buyers are interested in")
	public void user_will_see_a_list_of_products_that_buyers_are_interested_in() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Verify Content Diminati'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.pressBack()
		WebUI.callTestCase(findTestCase('Pages/Home/Click Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Then("User click Terjual Page and User will see a list of products that have been successfully sold")
	public void user_click_Terjual_Page_and_User_will_see_a_list_of_products_that_have_been_successfully_sold() {
		WebUI.callTestCase(findTestCase('Pages/View Product/Click Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.pressBack()
		WebUI.callTestCase(findTestCase('Pages/Home/Click Keluar'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	//	@Then("User will see a list of products that have been successfully sold")
	//	public void user_will_see_a_list_of_products_that_have_been_successfully_sold() {
	//		// Write code here that turns the phrase above into concrete actions
	//		throw new PendingException();
	//	}

}