#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@AddProduct
Feature: Add Product
  User wants to add products
  
  @ADP001
  Scenario: ADP001 - Users fill in the product name with valid data
    Given User already login to account
    #Then User click beranda page
    Then User click add product with + button
    When User will see add product form
    And User insert product name
    And User insert product price
    And User choose the category
    And insert seller location
    And User insert product description
    And User insert product image
    Then User click Terbitkan
    
  @ADP002
  Scenario: ADP002 - User did not fill in the product name column
    Given User already login to account
    #Then User click beranda page
    Then User click add product with + button
    When User will see add product form
    And User not insert a product name
    And User insert product price
    And User choose the category
    And insert seller location
    And User insert product description
    And User insert product image
    Then User click Terbitkan
    
  @ADP003
  Scenario: ADP003 - Users input product descriptions with long explanations
    Given User already login to account
    #Then User click beranda page
    Then User click add product with + button
    When User will see add product form
    And User insert product name
    And User insert product price
    And User choose the category
    And insert seller location
    And Users input product descriptions with long explanations
    And User insert product image
    Then User click Terbitkan
    
  @ADP004
  Scenario: ADP004 - User does not fill in the product price column
    Given User already login to account
    #Then User click beranda page
    Then User click add product with + button
    When User will see add product form
    And User insert product name
    And User not insert product price column
    And User choose the category
    And insert seller location
    And User insert product description
    And User insert product image
    Then User click Terbitkan
    

 