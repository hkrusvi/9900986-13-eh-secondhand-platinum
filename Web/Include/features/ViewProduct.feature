#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
 #(Comments)
#Sample Feature Definition Template
@ViewProduct
Feature: View Product
  User want to use several features on daftar jual saya page

  @VPR001
  Scenario: VPR001 - Users want to navigate to edit profile page from Daftar Jual Saya Page
    Given User already landing on Daftar Jual Saya page
    Then User Click Edit Profile button
    When User will see Edit Profile Textfield
    Then User Click and insert new profile photo
    And User insert new name "Hendri"
    And User choose new city "Jogja"
    And User input Address "Jalan Kenangan No.30"
    And User input mobile number
    Then User Click Simpan button
    
  @VPR002
  Scenario: VPR002 - Users want to add new products via "Daftar Jual Saya" page
    Given User already landing on Daftar Jual Saya page
    Then User click Tambah produk button 
    When User will see product detail textfield
    Then User insert Product Name
    And User insert Product Price
    And User Choose Product Category
    And User insert Product Description
    And User insert Product Image
    Then User click Terbitkan button
    
  @VPR003
  Scenario: VPR003 - Users want to see a list of products that buyers are interested in
    Given User already landing on Daftar Jual Saya page
    When User click Diminati button
    Then User will see a list of products that buyers are interested in
    
    
    
    

