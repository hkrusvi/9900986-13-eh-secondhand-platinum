#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@ViewProduct
Feature: View Product
  Users want to see products that have been published

  @VPR001
  Scenario: VPR001 - Users want to see all products that have been published
    Given User already login to account
    When User click Daftar Jual Saya
    Then Users will see products that have been successfully published
    
  @VPR002
  Scenario: VPR002 - Users want to see product data that buyers are interested in
    Given User already login to account
    When User click Daftar Jual Saya
    Then Users click Diminati Page
    And User will see a list of products that buyers are interested in
    
  @VPR003
  Scenario: VPR003 - Users want to see data on products that have been sold
    Given User already login to account
    When User click Daftar Jual Saya
    Then User click Terjual Page and User will see a list of products that have been successfully sold

    