package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {
	@Given("User already on REGISTER page")
	public void user_already_on_REGISTER_page() {
		WebUI.callTestCase(findTestCase('Pages/Register/Already On Register Page'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input valid name")
	public void user_input_valid_name() {
		WebUI.callTestCase(findTestCase('Pages/Register/Input Name'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input valid email")
	public void user_input_valid_email() {
		WebUI.callTestCase(findTestCase('Pages/Register/Input Email'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User input valid password")
	public void user_input_valid_password() {
		WebUI.callTestCase(findTestCase('Pages/Register/Input Password'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User click on DAFTAR button")
	public void user_click_on_DAFTAR_button() {
		WebUI.callTestCase(findTestCase('Pages/Register/Click Button Daftar'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User registered successfully")
	public void user_registered_successfully() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Session Success'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see Daftar Jual Saya icon")
	public void user_see_Daftar_Jual_Saya_icon() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Icon Daftar Jual Saya'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see Notification icon")
	public void user_see_Notification_icon() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Icon Notification'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see Akun icon")
	public void user_see_Akun_icon() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Icon Akun'), [:], FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.closeBrowser()
	}

	@Then("User can not register")
	public void user_can_not_register() {
		WebUI.callTestCase(findTestCase('Pages/Home/Verify Session Fail'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User stay on REGISTER page")
	public void user_stay_on_REGISTER_page() {
		WebUI.callTestCase(findTestCase('Pages/Register/Verify Register Page Title'), ['expectedPageTitle':'Daftar'], FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.closeBrowser()
	}

	@When("User input already registered email")
	public void user_input_already_registered_email() {
		WebUI.callTestCase(findTestCase('Pages/Register/Input Registered Email'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see error messages {string}")
	public void user_see_error_messages(String errorMessages) {
		WebUI.callTestCase(findTestCase('Pages/Register/Verify Error Messages'), ['expectedErrorMessages': errorMessages], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@When("User click Masuk link")
	public void user_click_Masuk_link() {
		WebUI.callTestCase(findTestCase('Pages/Register/Click Link Masuk'), [:], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see LOGIN page")
	public void user_see_LOGIN_page() {
		WebUI.callTestCase(findTestCase('Pages/Login/Verify Login Page Title'), ['expectedPageTitle':'Masuk'], FailureHandling.CONTINUE_ON_FAILURE)
	}

	@Then("User see LOGIN form")
	public void user_see_LOGIN_form() {
		WebUI.callTestCase(findTestCase('Pages/Login/Verify Login Form'), [:], FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.closeBrowser()
	}
}