$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/AddProduct.feature");
formatter.feature({
  "name": "Add Product",
  "description": "  User wants to add products",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@AddProduct"
    }
  ]
});
formatter.scenario({
  "name": "ADP001 - Users fill in the product name with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP002 - User did not fill in the product name column",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert a product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_not_insert_a_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP003 - Users input product descriptions with long explanations",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product price",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users input product descriptions with long explanations",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.users_input_product_descriptions_with_long_explanations()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP004 - User does not fill in the product price column",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@ADP004"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click add product with + button",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_add_product_with_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see add product form",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_will_see_add_product_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product name",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User not insert product price column",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_not_insert_product_price_column()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User choose the category",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_choose_the_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "insert seller location",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.insert_seller_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product description",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User insert product image",
  "keyword": "And "
});
formatter.match({
  "location": "AddProduct.user_insert_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terbitkan",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_click_Terbitkan()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/EditProfile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "  User wants to edit profile",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@EditProfile"
    }
  ]
});
formatter.scenario({
  "name": "EDP001 - Users want to change information on their profile with complete data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Name",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Mobile Number",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Mobile_Number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert City Name",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_City_Name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click and insert Address",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_click_and_insert_Address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP002 - User wants to change profile but data is incomplete",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the username form but does not fill in the form",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_clicks_on_the_username_form_but_does_not_fill_in_the_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "A notification will appear that the form must be filled in",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.a_notification_will_appear_that_the_form_must_be_filled_in()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP003 - Users fill in the cellphone number column with letters",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users fill in the cellphone number column with letters",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.users_fill_in_the_cellphone_number_column_with_letters()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EDP004 - User fills in the name column with numbers",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EDP001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Edit Profile icon",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_click_Edit_Profile_icon()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see edit profile form",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.users_will_see_edit_profile_form()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills in the name column with numbers",
  "keyword": "And "
});
formatter.match({
  "location": "EditProfile.user_fills_in_the_name_column_with_numbers()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see the profile that has been successfully changed",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.user_will_see_the_profile_that_has_been_successfully_changed()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/ViewProduct.feature");
formatter.feature({
  "name": "View Product",
  "description": "  Users want to see products that have been published",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@ViewProduct"
    }
  ]
});
formatter.scenario({
  "name": "VPR001 - Users want to see all products that have been published",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR001"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users will see products that have been successfully published",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.users_will_see_products_that_have_been_successfully_published()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR002 - Users want to see product data that buyers are interested in",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR002"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Users click Diminati Page",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.users_click_Diminati_Page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see a list of products that buyers are interested in",
  "keyword": "And "
});
formatter.match({
  "location": "ViewProduct.user_will_see_a_list_of_products_that_buyers_are_interested_in()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "VPR003 - Users want to see data on products that have been sold",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@ViewProduct"
    },
    {
      "name": "@VPR003"
    }
  ]
});
formatter.step({
  "name": "User already login to account",
  "keyword": "Given "
});
formatter.match({
  "location": "ViewProduct.user_already_login_to_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Daftar Jual Saya",
  "keyword": "When "
});
formatter.match({
  "location": "ViewProduct.user_click_Daftar_Jual_Saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Terjual Page and User will see a list of products that have been successfully sold",
  "keyword": "Then "
});
formatter.match({
  "location": "ViewProduct.user_click_Terjual_Page_and_User_will_see_a_list_of_products_that_have_been_successfully_sold()"
});
formatter.result({
  "status": "passed"
});
});