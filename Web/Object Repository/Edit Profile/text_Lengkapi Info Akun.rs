<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Lengkapi Info Akun</name>
   <tag></tag>
   <elementGuidId>0efe90ce-ae15-4988-9845-bc31df459b1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.fs-6.fw-bold.text-center.position-absolute.top-50.start-50.translate-middle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0a5f1c8f-6cef-4f02-851f-1c58aadc132b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle</value>
      <webElementGuid>dd3b8a68-0d7c-421c-b4a9-16a435655e25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Lengkapi Info Akun
      </value>
      <webElementGuid>d6b019b7-ea15-4915-9dca-0349904b4497</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/nav[@class=&quot;navbar navbar-expand-lg navbar-light bg-white shadow fixed-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;fs-6 fw-bold text-center position-absolute top-50 start-50 translate-middle&quot;]</value>
      <webElementGuid>f5561949-2c4f-408e-b7df-9452403e9745</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::div[5]</value>
      <webElementGuid>dc00a248-972c-4e67-8ef8-6df0eccbfe4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/preceding::div[6]</value>
      <webElementGuid>ab6b39e5-ee68-4b6b-b0a8-ca51817abe16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lengkapi Info Akun']/parent::*</value>
      <webElementGuid>6c7f2b92-ac7d-4a64-9665-72af48025ac8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div</value>
      <webElementGuid>8fa1bdc2-3e45-44e2-a702-25b153361a0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Lengkapi Info Akun
      ' or . = '
        Lengkapi Info Akun
      ')]</value>
      <webElementGuid>7adf264d-0d15-4f36-815d-181c2fcd2133</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
