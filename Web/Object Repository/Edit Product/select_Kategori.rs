<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Kategori</name>
   <tag></tag>
   <elementGuidId>a43eb43d-b2ac-4e32-8ab7-c1c607aad7fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='product_category_id']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#product_category_id</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>e15e69ea-e47b-42cb-b29b-283446b5bfa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-select rounded-4 p-3</value>
      <webElementGuid>ca3a71ca-92a9-46a0-8a04-5225efbb6e4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>ab20f859-6040-4eba-9da1-b85f13fa1426</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>product[category_id]</value>
      <webElementGuid>dbcc41eb-4167-4005-a613-303c13cfd531</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>product_category_id</value>
      <webElementGuid>687d7899-0ee8-4f9b-a5ab-0f5a3a36e1ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan</value>
      <webElementGuid>3f156743-e66c-461e-b57c-a54c76256c3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;product_category_id&quot;)</value>
      <webElementGuid>82664ea4-5b3f-4ecb-9210-7a29d43b0724</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='product_category_id']</value>
      <webElementGuid>abec245e-d518-45ac-ad67-78d96524d0b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::select[1]</value>
      <webElementGuid>bf4163d4-b475-4c88-8312-1061dde7c500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Produk'])[1]/following::select[1]</value>
      <webElementGuid>94908774-48ea-45a2-adf9-a735d55e35a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[1]/preceding::select[1]</value>
      <webElementGuid>bbd8727d-427f-4cf0-b93c-927fd020a539</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='truk'])[1]/preceding::select[1]</value>
      <webElementGuid>420a516b-98b6-41e7-afe7-984b013685fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>2a3ab9ba-eb24-45af-9eb8-c739534f684c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'product[category_id]' and @id = 'product_category_id' and (text() = 'Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan' or . = 'Pilih Kategori
Hobi
Kendaraan
Baju
Elektronik
Kesehatan')]</value>
      <webElementGuid>da787033-4ddb-4274-bfa7-ad192dda0108</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
