<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_TambahProduk</name>
   <tag></tag>
   <elementGuidId>543d7256-c07b-4425-bce9-ff10466e9a27</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a/div/i</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.bi.bi-plus.display-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>bc99a352-d509-4227-ad5e-ae312ecbdbba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bi bi-plus display-3</value>
      <webElementGuid>21d29a69-f9a0-4958-841d-2880d9c0db6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row my-5&quot;]/div[@class=&quot;col-12 col-lg-9 mt-5 mt-lg-0&quot;]/div[@class=&quot;row g-4&quot;]/div[@class=&quot;col-12 col-lg-4&quot;]/a[@class=&quot;new-product-button h-100 w-100 border-2 rounded-4 text-black-50 d-flex align-items-center justify-content-center&quot;]/div[@class=&quot;text-center&quot;]/i[@class=&quot;bi bi-plus display-3&quot;]</value>
      <webElementGuid>266cc9d6-cee0-48d5-b325-7e196f00fb58</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/i</value>
      <webElementGuid>6c82d9fa-53ec-4714-9bf0-b0b9fa9044f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
