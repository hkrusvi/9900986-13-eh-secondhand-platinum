#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Register
Feature: Register
	User want to register to SecondHand website
	
	@REG001
	Scenario: REG001 - User want to register using valid credentials 
		Given User already on REGISTER page
		When User input valid name
		And User input valid email
		And User input valid password
		And User click on DAFTAR button
		Then User registered successfully
		And User see Daftar Jual Saya icon
		And User see Notification icon
		And User see Akun icon
		
	@REG002 
	Scenario: REG002 - User want to register with leaving Name field empty
		Given User already on REGISTER page
		When User input valid email
		And User input valid password
		And User click on DAFTAR button
		Then User can not register
		And User stay on REGISTER page
		
	@REG003 
	Scenario: REG003 - User want to register with leaving Email field empty
		Given User already on REGISTER page
		When User input valid name
		And User input valid password
		And User click on DAFTAR button
		Then User can not register
		And User stay on REGISTER page
		
	@REG004 
	Scenario: REG004 - User want to register using already registered email
		Given User already on REGISTER page
		When User input valid name
		And User input already registered email
		And User input valid password
		And User click on DAFTAR button
		Then User see error messages "Email has already been taken"
		And User can not register
		And User stay on REGISTER page
		
	@REG005 
	Scenario: REG005 - User want to register with leaving Password field empty
		Given User already on REGISTER page
		When User input valid name
		And User input valid email
		And User click on DAFTAR button
		Then User can not register
		And User stay on REGISTER page
				
	@REG006
	Scenario: REG006 - User want to navigate from Register Page to Login Page 
		Given User already on REGISTER page
		When User click Masuk link
		Then User see LOGIN page
		And User see LOGIN form